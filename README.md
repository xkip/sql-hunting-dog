# README #

This is a source code for [SQL Hunting Dog](http://www.sql-hunting-dog.com)

### What is this repository for? ###

* For curious minds who want to explore how SQL Hunting Dog works
* For those who want to contribute

### How do I get set up? ###

* Clone repository
* Build
* You will need Visual Studio 2010


### Contribution guidelines ###

* Create a separate branch and then generate a merge request